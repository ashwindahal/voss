package com.voss.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AutomationExcerciseLoginPage {
	// Creating Webdriver obj
	WebDriver driver = null;

	public AutomationExcerciseLoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// Locators
	@FindBy(how = How.XPATH, using = "//input[@id='user[email]']")
	public WebElement emailField;

	@FindBy(how = How.XPATH, using = "//input[@id='user[password]']")
	public WebElement password;

	@FindBy(how = How.XPATH, using = "//input[@value='Sign in']")
	public WebElement submitBtn;

	public void Login(String userName, String pwd) {
		emailField.sendKeys(userName);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		password.sendKeys(pwd);
	}

	public void loginClick() {
		submitBtn.click();
	}

}
