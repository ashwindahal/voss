package com.voss.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AutomationExerciseFakePricingPage {

	// Creating Webdriver obj
	WebDriver driver = null;

	public AutomationExerciseFakePricingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// Locators
	@FindBy(how = How.XPATH, using = "//input[@id='user[email]']")
	public WebElement basicPackage;

}
