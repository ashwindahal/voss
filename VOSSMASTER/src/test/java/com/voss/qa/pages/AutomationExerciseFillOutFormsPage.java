package com.voss.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AutomationExerciseFillOutFormsPage {

	// Creating Webdriver obj
	WebDriver driver = null;

	// Locators
	By nameField1 = By.xpath("//input[@id='et_pb_contact_name_0']");
	By messageField1 = By.xpath("//textarea[@id='et_pb_contact_message_0']");
	By submit1 = By.xpath("(//button[@name='et_builder_submit_button'])[1]");
	By successMessage1 = By.xpath("(//div[@class='et-pb-contact-message'])[1]");

	By nameField2 = By.xpath("//input[@id='et_pb_contact_name_1']");
	By messageField2 = By.xpath("//textarea[@id='et_pb_contact_message_1']");
	By addValue = By.xpath("//input[@class='input et_pb_contact_captcha']");
	By submit2 = By.xpath("//button[contains(@name,'et_builder_submit_button')]");
	By successMessage2 = By.xpath("(//div[@class='et-pb-contact-message'])[2]");

	// Creating constructor for obj creation and maintaining driver
	public AutomationExerciseFillOutFormsPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterName1(String name) {
		driver.findElement(nameField1).sendKeys(name);
	}

	public void enterMessage1(String message) {
		driver.findElement(messageField1).sendKeys(message);
	}

	public void clickSubmit1() {
		driver.findElement(submit1).click();
	}

	public void enterName2(String sum) {
		driver.findElement(nameField2).sendKeys(sum);
	}

	public void enterMessage2(String message) {
		driver.findElement(messageField2).sendKeys(message);
	}

	public void clickSubmit2() {
		driver.findElement(submit2).submit();
	}

	public void enterValue(String value) {
		driver.findElement(addValue).sendKeys(value);
	}

}
