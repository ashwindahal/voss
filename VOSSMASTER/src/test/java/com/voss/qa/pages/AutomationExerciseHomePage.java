package com.voss.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AutomationExerciseHomePage {

	// Creating Webdriver obj
	WebDriver driver = null;

	// Locators
	By loginAutomationLink = By.xpath("//a[@href='http://courses.ultimateqa.com/users/sign_in']");
	By fillOutFormsLink = By.xpath("//a[@href='https://ultimateqa.com/filling-out-forms/']");

	// Creating constructor for obj creation and maintaining driver
	public AutomationExerciseHomePage(WebDriver driver) {
		this.driver = driver;
	}

	public void clikOnLoginAutomation() {
		driver.findElement(loginAutomationLink).click();
	}

	public void clikOnFillOutForms() {
		driver.findElement(fillOutFormsLink).click();
	}

}
