package com.voss.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PricingPageClassObject {

	WebDriver driver = null;

	WebElement element = null;

	By PricingPgTab = By.xpath("//a[contains(text(),'Fake Pricing Page')]");

	By validatingPricingPg = By.xpath("//h2[contains(text(),'Frequently Asked Questions')]");

	By browseDoc = By.xpath("//a[@class='et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light']");

	public PricingPageClassObject(WebDriver driver) {

		this.driver = driver;
	}

	public void PricingPageTab() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		driver.findElement(PricingPgTab).click();
		TimeUnit.SECONDS.sleep(3);

	}

	public void validatingPricingPage() {
		WebElement validate = driver.findElement(validatingPricingPg);

		if (validate.getText().equalsIgnoreCase("Frequently Asked Questions")) {
			System.out.println("PASS--Expected Message ---'Pricing  Page'-- is displayed as expected");
		}

		else {
			System.out.println("  Pricing  Page Did not displayed  ");

		}

	}

	public void scrollPage() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(browseDoc);
		js.executeScript("window.scrollBy(0,1400)", "");
		TimeUnit.SECONDS.sleep(5);
		js.executeScript("arguments[0].scrollIntoView();", Element);
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(browseDoc).sendKeys(Keys.ENTER);
		TimeUnit.SECONDS.sleep(3);
		driver.navigate().back();

	}

}
