package com.voss.qa.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.voss.qa.pages.AutomationExcerciseLoginPage;
import com.voss.qa.pages.AutomationExerciseFillOutFormsPage;
import com.voss.qa.pages.AutomationExerciseHomePage;
import com.voss.qa.pages.PricingPageClassObject;
import com.voss.qa.util.ScreenshotUtility;

import config.PropertiesFile;

public class AutomationExercisePageTest {
	// Class variables
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	WebDriver driver = null;
	public static String browserName = null;

	@BeforeSuite
	public void setUpTest() {
		// Creating ExtenReporter Object and creating new extentReport Html file
		// Create ExtentReports and attach repor ter(s)
		htmlReporter = new ExtentHtmlReporter("AutomationPractice.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}

	@BeforeTest
	public void setUpBrowser() {
		// Reads configuration file and invokes driver based on the configuration
		// Creating a obj of Properties to get the properties currently configured
		PropertiesFile.getProperties();
		ExtentTest test = extent.createTest("Invoke Browser", "This test is to validate that the browser is invoked");

		if (browserName.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
			driver = new ChromeDriver();
			test.log(Status.INFO, browserName + "Is invoked as Expected");

		} else if (browserName.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.gecko.driver", ".\\libs\\geckodriver.exe");
			driver = new FirefoxDriver();
			test.log(Status.INFO, browserName + " Is invoked as Expected");
		}

	}

	@Test(priority = 0)
	public void VOSSTest1PageTitle() throws InterruptedException {
		// Invoke browser for and navigate to web browser
		driver.get("https://ultimateqa.com/automation/");
		// Maximize the Browser
		driver.manage().window().maximize();
		// Adding waite for 10 Seconds to let browser load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Taking Screenshot
		ScreenshotUtility.takeSnapShot(driver, "Automation Practice Homepage");

		// Validating the Title
		String pageTitle = driver.getTitle();
		String expectedResult = "Automation Practice - Ultimate QA";

		// Creating a Test Report for
		ExtentTest isPageTitleAsExpected = extent.createTest("Validate Page Title",
				"This test is to validate that the title is printing as expected");

		if (pageTitle.contains(expectedResult)) {
			isPageTitleAsExpected.pass("Page title with " + pageTitle + " is displaying as expected");
		} else {
			isPageTitleAsExpected.fail("Page title is not same as Expected");
		}

	}

	@Test(priority = 1)
	public void VOSSTest2LoginLogOut() throws InterruptedException {
		// Navigating to home page
		navigateToHomePage();
		AutomationExerciseHomePage homePageObj = new AutomationExerciseHomePage(driver);
		homePageObj.clikOnLoginAutomation();

		ScreenshotUtility.takeSnapShot(driver, "Automation Practice Login Page");

		String loginPageTitle = driver.getTitle();
		String ExpectedTitle = "Ultimate QA";

		ExtentTest loginPageTest = extent.createTest("Validate we are in login Page",
				"This test is to validate that we are in login page");

		if (loginPageTitle.contains(ExpectedTitle)) {
			loginPageTest.pass("Expected page " + loginPageTitle + " is displayed");
		} else {
			loginPageTest.fail("Expected page " + loginPageTitle + " is not displayed");
		}

		AutomationExcerciseLoginPage loginPage = new AutomationExcerciseLoginPage(driver);
		loginPage.Login("ashwin.nepal1995@gmail.com", "TestTester");
		loginPage.loginClick();

		ExtentTest completeLogin = extent.createTest("Validate Login fileds",
				"Test to validate login field has bee filled");

		if (driver.findElement(By.xpath("//input[@id='user[email]']")).getText() != null) {
			completeLogin.pass("Email filled");
		}
		if (driver.findElement(By.xpath("//input[@id='user[password]']")).getText() != null) {
			completeLogin.pass("Password filled");
		}

		// Return to Homepage
		navigateToHomePage();
		completeLogin.pass("Logged out");

	}

	@Test(priority = 2)
	public void VOSSTest3FillOutForms() throws InterruptedException {
		// Clicking on Fill out Forms link
		AutomationExerciseHomePage homePageObj = new AutomationExerciseHomePage(driver);
		homePageObj.clikOnFillOutForms();

		ScreenshotUtility.takeSnapShot(driver, "Automation Practice Fill Forms Page");

		// Entering both name and Message Fields
		AutomationExerciseFillOutFormsPage obj = new AutomationExerciseFillOutFormsPage(driver);

		// Fill Form 1
		obj.enterName1("Test1");
		obj.enterMessage1("This is a Test1 message");

		obj.clickSubmit1();
		TimeUnit.SECONDS.sleep(2);
		obj.clickSubmit1();
		TimeUnit.SECONDS.sleep(2);
		obj.clickSubmit1();
		TimeUnit.SECONDS.sleep(2);

		// Fill Form 2
		obj.enterName2("Test2");
		obj.enterMessage2("This is a Test2 Message");

		// Getting the text from page and generating sum
		String numValue1 = driver.findElement(By.xpath("//span[@class='et_pb_contact_captcha_question']")).getText();
		String removespace = numValue1.replaceAll("\\s+", "");
		// get two numbers
		String[] parts = removespace.split("\\+");
		String firstPart = parts[0];
		String secondPart = parts[1];
		String[] parts1 = secondPart.split("\\=");
		String lastPart = parts1[0];
		int summation = Integer.parseInt(firstPart) + Integer.parseInt(lastPart);

		String s = String.valueOf(summation);
		obj.enterValue(s);

		obj.clickSubmit2();

		String expectedMessage1 = "Form filled out successfully";
		ExtentTest formFill1 = extent.createTest("Validate From 1", "Test to validate from 1");

		if (driver.findElement(By.xpath("(//div[@class='et-pb-contact-message'])[1]")).getText()
				.contains(expectedMessage1)) {
			formFill1.pass("Expected message '" + expectedMessage1 + "' is displayed as expected for first form");
		} else {
			formFill1.fail("Expected message '" + expectedMessage1 + "' is not displayed for first form");
		}

		TimeUnit.SECONDS.sleep(2);

		String expectedMessage2 = "Success";
		ExtentTest formFill2 = extent.createTest("Validate From 2", "Test to validate from 2");

		if (driver.findElement(By.xpath("(//div[@class='et-pb-contact-message'])[2]")).getText()
				.contains(expectedMessage2)) {
			formFill2.pass("Expected message '" + expectedMessage2 + "' is displayed as expected for first form");
		} else {
			formFill2.fail("Expected message '" + expectedMessage2 + "' is not displayed for first form");
		}

		// Return to homepage
		navigateToHomePage();

	}

	@Test(priority = 4)
	public void VOSSTest4FakePriceingPage() throws InterruptedException {
		// Creating an object for LandingPageClassObject,
		PricingPageClassObject obj = new PricingPageClassObject(driver);
		ScreenshotUtility.takeSnapShot(driver, "Automation Practice Fake Priceing Page");
		obj.PricingPageTab();
		obj.validatingPricingPage();
		obj.scrollPage();

		ExtentTest fakePricingPage = extent.createTest("Land at fake pricing page",
				"Test to check if we are at fake princing page");
		fakePricingPage.pass("Successfully navigated to Fake priceing page an selected basic package!");

	}

	public void navigateToHomePage() {
		driver.get("https://ultimateqa.com/automation/");
	}

	@AfterTest
	public void tearDownBrowser() {
		// close browser
		// driver.quit();
		System.out.println("------------------------------Tests Compleated-------------------------------------");
	}

	@AfterSuite
	public void tearDownTest() {
		driver.close();
		// caling the flush writes everything to the log file
		extent.flush();

	}

	public boolean isElementPresent(By by) {
		boolean check = false;

		try {
			driver.findElement(by);
			check = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return check;

	}

}
