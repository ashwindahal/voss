package config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.voss.qa.tests.AutomationExercisePageTest;

public class PropertiesFile {
	// 1. Create a object of class Properties class
	static Properties prop = new Properties();
	// Get the system project path
	static String projectPath = System.getProperty("user.dir");

	public static void getProperties() {
		try {
			InputStream input = new FileInputStream(projectPath + "/src/test/java/config/config.properties");
			// 3.Load Properties file
			prop.load(input);
			// 4. Get Values from Properties file
			String browser = prop.getProperty("browser");
			System.out.println(browser + " is invoked");
			//Setting the browser for the perticular test class
			AutomationExercisePageTest.browserName = browser;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}

	}

	public static void setProperties() {
		try {
			// Set Data to properties file
			// 1 create properties object(Moved at Global Level)
			// 2 Create a object of class OutputStream
			OutputStream output = new FileOutputStream(projectPath + "/src/test/java/config/config.properties");
			// 3 set values
			//prop.setProperty("browser", "chrome");
			prop.setProperty("Result", "Pass");
			// Store values in properties file
			prop.store(output, null);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}

	}

}
